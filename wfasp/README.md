# wfasp

Scrum team project, whose name is Weather Forecast And Storm Prediction and it is implemented with the use of Flutter. This project is carried out as part of the Team Project classes at the Carpathian State University in Krosno. 

## Requires
Google Play version 12451000.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
