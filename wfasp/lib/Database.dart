import 'dart:io';
import 'dart:core';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import 'dart:convert';

User userFromJson(String str) {
  final jsonData = json.decode(str);
  return User.fromMap(jsonData);
}

String userToJson(User data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class User {
  int id;
  String name;
  String password;
  bool remember_password;
  String units_type;

  User(
      {this.id,
      this.name,
      this.password,
      this.remember_password,
      this.units_type});

  factory User.fromMap(Map<String, dynamic> json) => new User(
      id: json["id"],
      password: json["password"],
      name: json["name"],
      remember_password: json["remember_password"],
      units_type: json["unit_type"]);

  Map<String, dynamic> toMap() => {
        "id": id,
        "password": password,
        "name": name,
        "remember_password": remember_password,
        "units_type": units_type
      };
}
