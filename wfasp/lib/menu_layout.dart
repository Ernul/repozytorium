import 'package:flutter/material.dart';
import 'package:wfasp/notyfication.dart';
import 'package:wfasp/my_localization_layout.dart';
import 'package:wfasp/settings_layout.dart';
import 'variables.dart';

class MenuDashbord extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        title: Text("Menu", style: headTextStyle),
        centerTitle: true,
        backgroundColor: Theme.of(context).appBarTheme.color,
        leading: IconButton(
            icon: Container(
              child: Image(
                image: AssetImage("assets/Images/Logo.png"),
                fit: BoxFit.cover,
              ),
              height: 100,
              width: 100,
            ),
            iconSize: 70,
            alignment: Alignment.topLeft,
            onPressed: () {
              Navigator.of(context).pop();
            }),
      ),
      body: Center(child: menu(context)),
      bottomNavigationBar: BottomAppBar(
        child: IconButton(
          icon: Icon(Icons.arrow_back,
              color: Theme.of(context).buttonColor, semanticLabel: 'Wstecz'),
          iconSize: 50,
          padding: EdgeInsets.all(5),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
    );
  }

  Widget menu(context) {
    return Padding(
        padding: EdgeInsets.all(5),
        child: Align(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              const SizedBox(
                height: 10,
              ),
              SizedBox(
                width: double.infinity,
                height: 60,
                child: FlatButton(
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => MyLocalizationDashboard()));
                    },
                    padding: EdgeInsets.all(0.0),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Theme.of(context).buttonColor,
                      ),
                      padding: EdgeInsets.all(8),
                      child: const Text(
                        'Moja lokalizacja',
                        style: headTextStyle,
                        maxLines: 1,
                      ),
                    )),
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                width: double.maxFinite,
                height: 60,
                child: FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  }, //Akcja dla mapy
                  child: Container(
                    decoration: BoxDecoration(
                      color: Theme.of(context).buttonColor,
                    ),
                    padding: EdgeInsets.all(8),
                    child: const Text(
                      'Mapa',
                      style: headTextStyle,
                      maxLines: 1,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                width: double.infinity,
                height: 60,
                child: FlatButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => SettingsDashbord()));
                  },
                  padding: EdgeInsets.all(0.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Theme.of(context).buttonColor,
                    ),
                    padding: EdgeInsets.all(8),
                    child: const Text(
                      'Ustawienia',
                      style: headTextStyle,
                      maxLines: 1,
                    ),
                  ),
                ),
              ),
              // Powiadomienie test
              FlatButton(
                color: Colors.white,
                child: Text(
                  "Powiadomienie TEST",
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                onPressed: () {
                  final Notifications not = new Notifications();
                  print("ODPALA SIĘ");
                  not.initNotifications();
                  not.pushNotifications("Test", "Powiadomień test wesoły.");
                  Icon(Icons.airline_seat_flat);
                },
              ),
            ],
          ),
        ));
  }
}
