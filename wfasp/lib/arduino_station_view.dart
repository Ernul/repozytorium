import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:sqflite/utils/utils.dart';
import 'variables.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:http/http.dart' as http;

List<dynamic> arduino_stations_list = null;
Map<int, ArduinoStation> arduino_station_collection = {};
Map<int, dynamic> temp_plot_points_list = {};
Map<int, dynamic> humid_plot_points_list = {};
Map<int, dynamic> pressure_plot_points_list = {};
List<bool> _isOpen = [];
bool zero_staion_assigned = false;

class ArduinoStation {
  int id;
  String name;
  Map<String, dynamic> actual_data;
  List<dynamic> historical_data;
  double latitude;
  double longtitude;

  ArduinoStation(this.id, this.name, this.actual_data, this.historical_data,
      this.latitude, this.longtitude);

  factory ArduinoStation.fromJson(dynamic json) {
    return ArduinoStation(
        json['id'] as int,
        json['name'] as String,
        json['actual_data'] as Map<String, dynamic>,
        json['historical_data'] as List<dynamic>,
        json['lat'] as double,
        json['lon'] as double);
  }
  @override
  String toString() {
    return '{ ${this.id}, ${this.name},${this.actual_data},${this.historical_data},${this.latitude},${this.longtitude}}';
  }
}

class Response {
  int id;
  List<dynamic> desc;

  Response(this.id, this.desc);

  factory Response.fromJson(dynamic json) {
    return Response(json[0]['ID'] as int, json[0]['desc'] as List<dynamic>);
  }

  @override
  String toString() {
    return '{ ${this.id}, ${this.desc} }';
  }
}

Future<Response> apiRequest(String url, Map jsonMap) async {
  HttpClient httpClient = new HttpClient();
  HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
  request.headers.add('content-type', 'application/json');
  request.add(utf8.encode(json.encode(jsonMap)));
  HttpClientResponse response = await request.close();
  if (response.statusCode == 200) {
    String reply = await response.transform(utf8.decoder).join();
    Response rr = Response.fromJson(jsonDecode(reply));
    httpClient.close();
    return rr;
  } else {
    zero_staion_assigned = true;
    throw Exception('Failed to load album');
  }
}

Future<void> load_arduino_stations() async {
  arduino_stations_list = null;
  arduino_station_collection = {};
  _isOpen = [];
  String url = "http://" +
      API_SERWER_IP +
      "/get_stations_info?email=" +
      username +
      "&api_key=" +
      GET_STATIONS_INFO_API_KEY;
  Map map = {};
  Response odp = await apiRequest(url, map);
  arduino_stations_list = odp.desc;
  for (var item in arduino_stations_list) {
    int id = item["id"];
    String name = item["name"];
    Map<String, dynamic> actual_data;
    List<dynamic> historical_data;

    if (item["actual_data"] != null) {
      actual_data = jsonDecode(item["actual_data"]);
    } else {
      actual_data = {};
    }
    if (item["historical_data"] != null) {
      historical_data = jsonDecode(item["historical_data"]);
    } else {
      historical_data = [];
    }
    double latitude = item["lat"];
    double longtitude = item["lon"];
    ArduinoStation arduinoStation = new ArduinoStation(
        id, name, actual_data, historical_data, latitude, longtitude);
    arduino_station_collection[arduino_station_collection.length] =
        arduinoStation;
    _isOpen.add(false);
    List<FlSpot> one_station_temp_plot_points = [];
    List<FlSpot> one_station_press_plot_point = [];
    List<FlSpot> one_station_humid_plot_points = [];
    int iterrator = 0;
    double lowest_temp_point = 50;
    double highest_temp_point = 0;
    double lowest_humid_point = 100;
    double highest_humid_point = 0;
    double lowest_pressure_point = 1200;
    double highest_pressure_point = 0;
    for (var item in historical_data) {
      if (item["temperature"] != null) {
        if (item["temperature"].toDouble() < lowest_temp_point)
          lowest_temp_point = item["temperature"].toDouble();

        if (item["humidity"].toDouble() < lowest_humid_point)
          lowest_humid_point = item["humidity"].toDouble();

        if (item["pressure"].toDouble() < lowest_pressure_point)
          lowest_pressure_point = item["pressure"].toDouble();

        if (item["temperature"].toDouble() > highest_temp_point)
          highest_temp_point = item["temperature"].toDouble();

        if (item["humidity"].toDouble() > highest_humid_point)
          highest_humid_point = item["humidity"].toDouble();

        if (item["pressure"].toDouble() > highest_pressure_point)
          highest_pressure_point = item["pressure"].toDouble();
      }

      one_station_temp_plot_points
          .add(FlSpot(iterrator.toDouble(), item["temperature"].toDouble()));
      one_station_humid_plot_points
          .add(FlSpot(iterrator.toDouble(), item["humidity"].toDouble()));
      one_station_press_plot_point
          .add(FlSpot(iterrator.toDouble(), item["pressure"].toDouble()));
      iterrator += 1;
    }
    print("LTP:" +
        lowest_temp_point.toString() +
        "HTP:" +
        highest_temp_point.toString());
    temp_plot_points_list[item["id"]] = {
      "points": one_station_temp_plot_points,
      "min_value": lowest_temp_point - 2,
      "max_value": highest_temp_point + 2
    };
    humid_plot_points_list[item["id"]] = {
      "points": one_station_humid_plot_points,
      "min_value": lowest_humid_point - 2,
      "max_value": highest_humid_point + 2
    };
    pressure_plot_points_list[item["id"]] = {
      "points": one_station_press_plot_point,
      "min_value": lowest_pressure_point - 2,
      "max_value": highest_pressure_point + 2
    };

    // temp_plot_points_list[item["id"]] = one_station_temp_plot_points;
    // humid_plot_points_list[item["id"]] = one_station_humid_plot_points;
    // pressure_plot_points_list[item["id"]] = one_station_press_plot_point;
    // if (temp_plot_points_list[item["id"]].length != 0) {
    //   print(temp_plot_points_list[item["id"]]);
    // }
  }
  print(temp_plot_points_list[3]["points"]);
}

class StationView extends StatefulWidget {
  @override
  _StationViewState createState() => _StationViewState();
}

class _StationViewState extends State<StationView> {
  @override
  void initState() {
    super.initState();
    load_arduino_stations().then((result) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        title: Text("Twoje stacje", style: headTextStyle),
        centerTitle: true,
        backgroundColor: Theme.of(context).appBarTheme.color,
        leading: IconButton(
            icon: Container(
              child: Image(
                image: AssetImage("assets/Images/Logo.png"),
                fit: BoxFit.cover,
              ),
              height: 100,
              width: 100,
            ),
            iconSize: 70,
            alignment: Alignment.topLeft,
            onPressed: () {
              Navigator.of(context).pop();
            }),
      ),
      body: menu(context),
      bottomNavigationBar: BottomAppBar(
        child: IconButton(
          icon: Icon(Icons.arrow_back,
              color: Theme.of(context).buttonColor, semanticLabel: 'Wstecz'),
          iconSize: 50,
          padding: EdgeInsets.all(5),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
    );
  }

  Widget menu(context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          if (arduino_stations_list != null)
            Column(
              children: [
                ExpansionPanelList(
                    children: [
                      for (var entry in arduino_station_collection.entries)
                        ExpansionPanel(
                            canTapOnHeader: true,
                            headerBuilder: (context, isOpen) {
                              return Padding(
                                  padding: EdgeInsets.all(10),
                                  child: Container(
                                      child: Text(entry.value.name,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18))));
                            },
                            body: Padding(
                              padding: EdgeInsets.all(10),
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        "Aktualne dane:",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 19),
                                      )
                                    ],
                                  ),
                                  Divider(
                                    color: Colors.black,
                                  ),
                                  Row(
                                    children: [
                                      Image.asset(
                                        "assets/Images/thermometer.png",
                                        width: 50,
                                        height: 50,
                                      ),
                                      SizedBox(
                                        width: 20,
                                      ),
                                      Text(
                                        entry.value.actual_data["temperature"]
                                                .toString() +
                                            "  °C",
                                        style: TextStyle(fontSize: 20),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Image.asset(
                                          "assets/Images/pressure-gauge.png",
                                          height: 50,
                                          width: 50),
                                      SizedBox(
                                        width: 20,
                                      ),
                                      Text(
                                          entry.value.actual_data["pressure"]
                                                  .toString() +
                                              "  hPa",
                                          style: TextStyle(fontSize: 20)),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Image.asset("assets/Images/humidity.png",
                                          height: 50, width: 50),
                                      SizedBox(
                                        width: 20,
                                      ),
                                      Text(
                                          entry.value.actual_data["humidity"]
                                                  .toString() +
                                              "  %",
                                          style: TextStyle(fontSize: 20)),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        "Starsze dane:",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 19),
                                      )
                                    ],
                                  ),
                                  Divider(
                                    color: Colors.black,
                                  ),
                                  if (temp_plot_points_list[entry.value.id]
                                              ["points"]
                                          .length >
                                      5)
                                    Column(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.all(10),
                                          child: Text("Temperatura"),
                                        ),
                                        SingleChildScrollView(
                                          scrollDirection: Axis.horizontal,
                                          child: Row(
                                            children: [
                                              LineChart(
                                                LineChartData(
                                                  lineTouchData: LineTouchData(
                                                      enabled: true),
                                                  lineBarsData: [
                                                    LineChartBarData(
                                                      spots:
                                                          temp_plot_points_list[
                                                                  entry
                                                                      .value.id]
                                                              ["points"],
                                                      isCurved: true,
                                                      barWidth: 5,
                                                      colors: [
                                                        Colors.redAccent,
                                                      ],
                                                      dotData: FlDotData(
                                                        show: false,
                                                      ),
                                                    ),
                                                  ],
                                                  maxY: temp_plot_points_list[
                                                          entry.value.id]
                                                      ["max_value"],
                                                  minY: temp_plot_points_list[
                                                          entry.value.id]
                                                      ["min_value"],
                                                  titlesData: FlTitlesData(
                                                    bottomTitles: SideTitles(
                                                        showTitles: true,
                                                        reservedSize: 1,
                                                        getTitles: (value) {
                                                          switch (
                                                              value.toInt()) {
                                                            case 6:
                                                              return '30m';
                                                            case 12:
                                                              return '1g';
                                                            case 36:
                                                              return '3g';
                                                            case 72:
                                                              return '6g';
                                                            case 144:
                                                              return '12g ';
                                                            case 288:
                                                              return '1 dzień ';
                                                            case 576:
                                                              return '2 dni';
                                                            case 864:
                                                              return '3 dni';
                                                            case 1152:
                                                              return '4 dni';
                                                            case 1440:
                                                              return '5 dni';
                                                            case 1728:
                                                              return '6 dni';
                                                            case 2016:
                                                              return '7 dni';
                                                            default:
                                                              return '';
                                                          }
                                                        }),
                                                    leftTitles: SideTitles(
                                                      showTitles: true,
                                                      margin: 1,
                                                      interval: 3,
                                                      getTitles: (value) {
                                                        String v = value
                                                            .toInt()
                                                            .toString();
                                                        return '${v} °C';
                                                      },
                                                    ),
                                                  ),
                                                  axisTitleData:
                                                      FlAxisTitleData(
                                                    leftTitle: AxisTitle(
                                                      showTitle: true,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.all(10),
                                          child: Text("Ciśnienie"),
                                        ),
                                        SingleChildScrollView(
                                          scrollDirection: Axis.horizontal,
                                          child: Row(
                                            children: [
                                              LineChart(
                                                LineChartData(
                                                  lineTouchData: LineTouchData(
                                                      enabled: true),
                                                  lineBarsData: [
                                                    LineChartBarData(
                                                      spots:
                                                          pressure_plot_points_list[
                                                                  entry
                                                                      .value.id]
                                                              ["points"],
                                                      isCurved: true,
                                                      barWidth: 5,
                                                      colors: [
                                                        Colors.blue,
                                                      ],
                                                      dotData: FlDotData(
                                                        show: false,
                                                      ),
                                                    ),
                                                  ],
                                                  maxY:
                                                      pressure_plot_points_list[
                                                              entry.value.id]
                                                          ["max_value"],
                                                  minY:
                                                      pressure_plot_points_list[
                                                              entry.value.id]
                                                          ["min_value"],
                                                  titlesData: FlTitlesData(
                                                    bottomTitles: SideTitles(
                                                        showTitles: false,
                                                        reservedSize: 12,
                                                        getTitles: (value) {
                                                          switch (
                                                              value.toInt()) {
                                                            case 6:
                                                              return '30m';
                                                            case 12:
                                                              return '1g';
                                                            case 36:
                                                              return '3g';
                                                            case 72:
                                                              return '6g';
                                                            case 144:
                                                              return '12g ';
                                                            case 288:
                                                              return '1 dzień ';
                                                            case 576:
                                                              return '2 dni';
                                                            case 864:
                                                              return '3 dni';
                                                            case 1152:
                                                              return '4 dni';
                                                            case 1440:
                                                              return '5 dni';
                                                            case 1728:
                                                              return '6 dni';
                                                            case 2016:
                                                              return '7 dni';
                                                            default:
                                                              return '';
                                                          }
                                                        }),
                                                    leftTitles: SideTitles(
                                                      showTitles: true,
                                                      margin: 10,
                                                      interval: 5,
                                                      getTitles: (value) {
                                                        String v = value
                                                            .toInt()
                                                            .toString();
                                                        return '${v} hPa';
                                                      },
                                                    ),
                                                  ),
                                                  axisTitleData:
                                                      FlAxisTitleData(
                                                    leftTitle: AxisTitle(
                                                      showTitle: true,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.all(10),
                                          child: Text("Wilgotność"),
                                        ),
                                        SingleChildScrollView(
                                          scrollDirection: Axis.horizontal,
                                          child: Row(
                                            children: [
                                              LineChart(
                                                LineChartData(
                                                  lineTouchData: LineTouchData(
                                                      enabled: true),
                                                  lineBarsData: [
                                                    LineChartBarData(
                                                      spots:
                                                          humid_plot_points_list[
                                                                  entry
                                                                      .value.id]
                                                              ["points"],
                                                      isCurved: true,
                                                      barWidth: 5,
                                                      colors: [
                                                        Colors.green,
                                                      ],
                                                      dotData: FlDotData(
                                                        show: false,
                                                      ),
                                                    ),
                                                  ],
                                                  maxY: 100,
                                                  minY: 0,
                                                  titlesData: FlTitlesData(
                                                    bottomTitles: SideTitles(
                                                        showTitles: false,
                                                        reservedSize: 10,
                                                        getTitles: (value) {
                                                          switch (
                                                              value.toInt()) {
                                                            case 6:
                                                              return '30m';
                                                            case 12:
                                                              return '1g';
                                                            case 36:
                                                              return '3g';
                                                            case 72:
                                                              return '6g';
                                                            case 144:
                                                              return '12g ';
                                                            case 288:
                                                              return '1 dzień ';
                                                            case 576:
                                                              return '2 dni';
                                                            case 864:
                                                              return '3 dni';
                                                            case 1152:
                                                              return '4 dni';
                                                            case 1440:
                                                              return '5 dni';
                                                            case 1728:
                                                              return '6 dni';
                                                            case 2016:
                                                              return '7 dni';
                                                            default:
                                                              return '';
                                                          }
                                                        }),
                                                    leftTitles: SideTitles(
                                                      showTitles: true,
                                                      margin: 5,
                                                      interval: 10,
                                                      getTitles: (value) {
                                                        String v = value
                                                            .toInt()
                                                            .toString();
                                                        return '${v} %';
                                                      },
                                                    ),
                                                  ),
                                                  axisTitleData:
                                                      FlAxisTitleData(
                                                    leftTitle: AxisTitle(
                                                      showTitle: true,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    )
                                  else
                                    Text(
                                        "Nie możemy wyświetlić wykresów ze względu na zbyt mają ilość danych")
                                ],
                              ),
                            ),
                            isExpanded: _isOpen[entry.key])
                    ],
                    expansionCallback: (i, isOpen) =>
                        setState(() => _isOpen[i] = !isOpen))
              ],
            ),
          if (zero_staion_assigned == true)
            Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    SizedBox(
                      height: 100,
                    ),
                    Text(
                      "Do twojego konta nie została dodana żadna stacja pogodowa",
                      textAlign: TextAlign.center,
                      softWrap: true,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Divider(
                      color: Colors.black,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Utwórz stację wpisując w instalatorze mail do konta a zostanie ona automatycznie do niego dodana, i będzie się tutaj wyświetlać. ",
                      style: TextStyle(fontSize: 16),
                      textAlign: TextAlign.center,
                    )
                  ],
                )),
        ],
      ),
    );
  }
}
