import 'package:flutter/foundation.dart';

class AppStateNotifier extends ChangeNotifier {
  bool isDarkMode = false;
  void updateTheme(bool isDarkModeOn) {
    this.isDarkMode = isDarkModeOn;
    notifyListeners();
  }
}
