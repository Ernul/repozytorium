import 'dart:io';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'variables.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

// ignore: non_constant_identifier_names
bool search_by_city = false;
String city = "Krosno";

class MyLocalizationDashboard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyLocalizationState();
}

class MyLocalizationState extends State<MyLocalizationDashboard> {
  Future<Dane> futureDane;
  // ignore: non_constant_identifier_names
  void reload_state() {
    futureDane = fetchAlbum();
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    reload_state();
  }

  Widget dashboard(context) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Visibility(
            visible: false,
            child: Column(
              children: [
                TextField(
                    onChanged: (text) {
                      city = text;
                    },
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Wpisz nazwę miasta',
                      labelStyle: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontSize: 22,
                          decoration: TextDecoration.underline),
                    )),
                TextButton(
                    onPressed: () {
                      search_by_city = true;
                      reload_state();
                    },
                    child: Icon(
                      Icons.arrow_forward,
                      size: 50,
                      color: Theme.of(context).iconTheme.color,
                    ))
              ],
            )),
        FutureBuilder<Dane>(
          future: futureDane,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              print(snapshot.data.icon);

              return Center(
                  child: Column(children: [
                SizedBox(
                  height: 10,
                ),
                Container(
                    decoration: BoxDecoration(
                      color: Colors.grey,
                      shape: BoxShape.circle,
                    ),
                    padding: EdgeInsets.all(80),
                    child: Column(
                      children: [
                        FittedBox(
                            child: Text(
                          snapshot.data.city,
                          style: TextStyle(
                              fontSize: 45,
                              fontWeight: FontWeight.bold,
                              letterSpacing: 2),
                        )),
                        Container(
                            child: Image(
                              width: 100,
                              image: AssetImage("assets/Images/" +
                                  snapshot.data.icon +
                                  ".png"),
                            ),
                            decoration: BoxDecoration(
                                color: Colors.blue, shape: BoxShape.circle)),
                        Text(
                          snapshot.data.desc,
                          style: TextStyle(fontSize: 17),
                        ),
                        Divider(
                          color: Colors.black,
                        ),
                        Text(
                          snapshot.data.temp.toString() + "°C",
                          style: TextStyle(fontSize: 27),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: <Widget>[
                            SizedBox(
                              width: 40,
                            ),
                            Image.asset("assets/Images/humidity.png",
                                height: 20, width: 20),
                            Text(
                              snapshot.data.humidity.toString() + "%",
                              style: TextStyle(fontSize: 20),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Image.asset("assets/Images/pressure-gauge.png",
                                height: 20, width: 20),
                            Text(
                              snapshot.data.pressure.toString() + " hPa",
                              style: TextStyle(fontSize: 20),
                            ),
                          ],
                        )
                      ],
                    )),
                Container(
                  margin: EdgeInsets.only(
                      left: 8.0, top: 8.0, bottom: 8.0, right: 12.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.0),
                    color: Colors.grey,
                    shape: BoxShape.rectangle,
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Wrap(
                              crossAxisAlignment: WrapCrossAlignment.center,
                              children: [
                                Image.asset("assets/Images/sunrise.png",
                                    height: 60, width: 60),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  snapshot.data.sunrise_h +
                                      ":" +
                                      snapshot.data.sunrise_m,
                                  style: TextStyle(fontSize: 30),
                                ),
                              ],
                            ),
                            Wrap(
                              crossAxisAlignment: WrapCrossAlignment.center,
                              children: [
                                Image.asset("assets/Images/sunset.png",
                                    height: 45, width: 45),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  snapshot.data.sunset_h +
                                      ":" +
                                      snapshot.data.sunset_m,
                                  style: TextStyle(fontSize: 30),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ]));
            } else if (snapshot.hasError) {
              return Column(children: [
                Text("${snapshot.error}"),
                TextButton(
                    onPressed: reload_state,
                    child: Icon(
                      Icons.replay,
                      color: Theme.of(context).iconTheme.color,
                      size: 50,
                    ))
              ]);
            }
            return CircularProgressIndicator(
              strokeWidth: 8,
              backgroundColor: Theme.of(context).primaryColor,
            );
          },
        ),
        TextButton(
            onPressed: () {
              search_by_city = false;
              reload_state();
            },
            child: Icon(
              Icons.refresh,
              size: 50,
              color: Theme.of(context).iconTheme.color,
            )),
      ],
    ));
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        title: Text("Moja Lokalizacja", style: headTextStyle),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(
            Icons.menu,
            color: Theme.of(context).iconTheme.color,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            dashboard(context),
          ],
        ),
      ),
    );
  }
}

String getFormattedDate(String date) {
  /// Convert into local date format.
  var localDate = DateTime.parse(date).toLocal();

  /// inputFormat - format getting from api or other func.
  /// e.g If 2021-05-27 9:34:12.781341 then format should be yyyy-MM-dd HH:mm
  /// If 27/05/2021 9:34:12.781341 then format should be dd/MM/yyyy HH:mm
  var inputFormat = DateFormat('yyyy-MM-dd HH:mm');
  var inputDate = inputFormat.parse(localDate.toString());

  /// outputFormat - convert into format you want to show.
  var outputFormat = DateFormat('dd/MM/yyyy HH:mm');
  var outputDate = outputFormat.format(inputDate);

  return outputDate.toString();
}

class Dane {
  final String city;
  final double temp;
  final int humidity;
  final int pressure;
  final String desc;
  final String icon;
  final String sunrise_h;
  final String sunrise_m;
  final String sunset_h;
  final String sunset_m;
  final int wind_speed;
  final int wind_deg;

  Dane(
      {this.city,
      this.temp,
      this.humidity,
      this.pressure,
      this.desc,
      this.icon,
      this.sunrise_h,
      this.sunrise_m,
      this.sunset_h,
      this.sunset_m,
      this.wind_speed,
      this.wind_deg});
  factory Dane.fromJson(Map<String, dynamic> json) {
    int sunrise_miliseconds = json['sys']['sunrise'] * 1000;
    int sunset_miliseconds = json['sys']['sunset'] * 1000;
    var sunrise_date =
        DateTime.fromMillisecondsSinceEpoch(sunrise_miliseconds, isUtc: true)
            .toLocal();
    var sunset_date =
        DateTime.fromMicrosecondsSinceEpoch(sunset_miliseconds, isUtc: true)
            .toLocal();
    return Dane(
        city: json['name'],
        temp: json['main']['temp'].toDouble(),
        humidity: json['main']['humidity'],
        pressure: json['main']['pressure'],
        desc: json['weather'][0]['description'],
        icon: json['weather'][0]['icon'],
        sunrise_h: sunrise_date.hour.toString(),
        sunrise_m: sunrise_date.minute.toString(),
        sunset_h: sunset_date.hour.toString(),
        sunset_m: sunset_date.minute.toString());
  }
}

Future<Dane> fetchAlbum() async {
  if (search_by_city == true) {
    final response = await http.post(
        'https://api.openweathermap.org/data/2.5/weather?q=' +
            city +
            '&appid=10c08f6dc9a5eea3ecf2b513dc77e0af&units=metric&lang=pl&mode=json');
    if (response.statusCode == 200) {
      return Dane.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load info from weather service');
    }
  } else {
    final response = await http.post(
        'https://api.openweathermap.org/data/2.5/weather?lat=' +
            lat.toString() +
            '&lon=' +
            lon.toString() +
            '&appid=7e122df6d87d7fe6d0514b35a3e4714c&units=metric&lang=pl&mode=json');
    if (response.statusCode == 200) {
      print(response.body.toString());
      return Dane.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load info from weather service');
    }
  }
}
