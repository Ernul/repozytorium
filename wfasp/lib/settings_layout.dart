import 'package:flutter/material.dart';
import 'app_state_notifier.dart';
import 'variables.dart';
import 'package:provider/provider.dart';

class SettingsDashbord extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SettingsDashBordState();
}

class SettingsDashBordState extends State<SettingsDashbord> {
  Widget menu(context) {
    return AppBar(
      title: Text("Ustawienia", style: headTextStyle),
      centerTitle: true,
      leading: IconButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        icon: Icon(
          Icons.menu,
          color: Theme.of(context).iconTheme.color,
        ),
      ),
    );
  }

  Widget dashboard(context) {
    return Padding(
      padding: EdgeInsets.all(80),
      child: Align(
        child: Column(
          children: <Widget>[
            const SizedBox(
              height: 30,
            ),
            SizedBox(
              height: 20,
              child: Text(
                'Zmiana motywu',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 18,
                    color: Theme.of(context).colorScheme.onPrimary),
                maxLines: 1,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Switch(
              value: Provider.of<AppStateNotifier>(context).isDarkMode,
              onChanged: (boolValue) {
                Provider.of<AppStateNotifier>(context, listen: false)
                    .updateTheme(boolValue);
              },
              focusColor: Theme.of(context).accentColor,
              activeColor: Theme.of(context).buttonColor,
            ),
            const SizedBox(
              height: 20,
            ),
            FlatButton(
                onPressed: () {
                  showAboutDialog(
                    context: context,
                    applicationVersion: '1.0.0',
                    applicationName: 'Weather Forecast And Storm Prediction',
                    //applicationIcon: AssetImage("assets/Images/Logo.png"),
                    applicationLegalese:
                        'Aplikacja przygotowana \n w ramach projektu zespołowego \ndo dowolnego użytku.\n© KPU Krosno',
                  );
                }, //Informacje o Aplikacji
                padding: EdgeInsets.all(1.0),
                color: Theme.of(context).backgroundColor,
                child: Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor.withOpacity(0.60),
                  ),
                  padding: EdgeInsets.all(10),
                  child: const Text(
                    'O Aplikacji',
                    style: headTextStyle,
                    maxLines: 1,
                  ),
                ))
          ],
        ),
      ),
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              menu(context),
              dashboard(context),
            ],
          ),
        ));
  }
}
