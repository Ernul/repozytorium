import 'package:flutter/material.dart';

class ThemeItems {
  ThemeItems._();
  static final ThemeData dark = ThemeData(
    accentColor: Colors.amber[400],
    backgroundColor: Colors.black12,
    brightness: Brightness.dark,
    buttonColor: Colors.green,
    cursorColor: Colors.white70,
    dialogBackgroundColor: Colors.black26,
    disabledColor: Colors.grey[800],
    errorColor: Colors.red[500],
    primaryColor: Colors.green[800],
    scaffoldBackgroundColor: Colors.black12,
    secondaryHeaderColor: Colors.green[500],
    iconTheme: IconThemeData(color: Colors.white, opacity: 0.9),
    textSelectionColor: Colors.white,
    appBarTheme: AppBarTheme(
      color: Colors.green[800],
      shadowColor: Colors.black54,
    ),
    colorScheme: ColorScheme.dark(
      primary: Colors.green[800],
      onPrimary: Colors.green,
      primaryVariant: Colors.green[600],
      secondary: Colors.green,
    ),
    cardTheme: CardTheme(
      color: Colors.amber,
    ),
  );
  static final ThemeData light = ThemeData(
    accentColor: Colors.black,
    backgroundColor: Colors.white70,
    brightness: Brightness.light,
    buttonColor: Colors.green[800],
    cursorColor: Colors.white70,
    dialogBackgroundColor: Colors.white54,
    disabledColor: Colors.grey[900],
    errorColor: Colors.red[500],
    primaryColor: Colors.green[800],
    scaffoldBackgroundColor: Colors.white10,
    secondaryHeaderColor: Colors.green[800],
    iconTheme: IconThemeData(color: Colors.white, opacity: 0.9),
    textSelectionColor: Colors.black,
    cardColor: Colors.white,
    appBarTheme:
        AppBarTheme(color: Colors.green[800], shadowColor: Colors.grey[800]),
    colorScheme: ColorScheme.light(
      primary: Colors.teal[800],
      onPrimary: Colors.teal,
      primaryVariant: Colors.teal[600],
      secondary: Colors.amber,
    ),
    cardTheme: CardTheme(
      color: Colors.green[800],
      clipBehavior: Clip.hardEdge,
      shadowColor: Colors.white30,
    ),
  );
}
