import 'dart:convert';
import 'dart:io';
//import 'dart:html';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:wfasp/app_state_notifier.dart';
import 'package:wfasp/menu_layout.dart';
import 'package:wfasp/variables.dart';
import 'package:wfasp/notyfication.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'app_state_notifier.dart';
import 'themes.dart';
import 'package:location/location.dart';
import 'package:http/http.dart' as http;
import 'login_register.dart';
import 'package:wfasp/arduino_station_view.dart';

void main() {
  runApp(
    ChangeNotifierProvider<AppStateNotifier>(
      create: (_) => new AppStateNotifier(),
      child: MyApp(),
    ),
  );
  _getCurrentLocation();
  location.onLocationChanged.listen((LocationData currentLocation) {
    lat = currentLocation.latitude;
    lon = currentLocation.longitude;
    //print("Lon: " + lon.toString() + " Lat: " + lat.toString());
  });
}

String id, desc;
String login_or_register = "login";

class MyApp extends StatelessWidget {
  //Root of Application.
  @override
  Widget build(BuildContext context) {
    return Consumer<AppStateNotifier>(builder: (context, appState, child) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'WFASP',
        theme: ThemeItems.light,
        darkTheme: ThemeItems.dark,
        themeMode: appState.isDarkMode ? ThemeMode.dark : ThemeMode.light,
        home: MyHomePage(title: 'WFASP'),
      );
    });
  }
}

void _getCurrentLocation() async {
  location = new Location();
  location.changeSettings(accuracy: LocationAccuracy.high, interval: 20000);
  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  LocationData _locationData;

  _serviceEnabled = await location.serviceEnabled();
  if (!_serviceEnabled) {
    _serviceEnabled = await location.requestService();
    if (!_serviceEnabled) {
      return;
    }
  }

  _permissionGranted = await location.hasPermission();
  if (_permissionGranted == PermissionStatus.denied) {
    _permissionGranted = await location.requestPermission();
    if (_permissionGranted != PermissionStatus.granted) {
      return;
    }
  }
  _locationData = await location.getLocation();

  lat = _locationData.latitude;
  lon = _locationData.longitude;
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  //Kontroler wysuwanego panelu
  final sliding_up_panel_controller = PanelController();
  // Funkcja wywołująca główny panel
  void Bottom_Panel_Builder() {
    if (sliding_up_panel_controller.isPanelClosed) {
      sliding_up_panel_controller.open();
    } else {
      sliding_up_panel_controller.close();
    }
  }

  Widget login_register_widget() {
    if (login_or_register == "login") {
      if (reset_password_box) {
        //Okno resetu hasła (w sensie zmiana z użyciem maila)
        return Column(
          children: [
            Text("Reset hasła"),
            TextField(
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
              decoration: InputDecoration(
                fillColor: Colors.white,
                filled: true,
                labelText: "Wprowadź mail",
              ),
              textAlign: TextAlign.center,
              onChanged: (text) {
                password_to_reset_mail = text;
              },
            ),
            FlatButton(
              color: Colors.white,
              textColor: Colors.green,
              child: Text(
                "Zresetuj  hasło",
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              ),
              onPressed: () {
                setState(() {
                  reset_password(password_to_reset_mail);
                });
              },
            ),
            IconButton(
                icon: Icon(Icons.arrow_back_ios),
                onPressed: () {
                  //Powrót do widoku zalogowanego usera
                  reset_password_box = false;
                  setState(() {});
                })
          ],
        );
      }
      //Widget logowania po zalogowaniu
      if (is_logged) {
        //Zmiana hasła
        if (change_password_box) {
          return Column(
            children: [
              TextField(
                obscureText: true,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
                decoration: InputDecoration(
                    fillColor: Colors.white,
                    filled: true,
                    labelText: "Wprowadź stare hasło"),
                textAlign: TextAlign.center,
                onChanged: (text) {
                  password_to_change_old_password = text;
                },
              ),
              TextField(
                obscureText: true,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
                decoration: InputDecoration(
                    fillColor: Colors.white,
                    filled: true,
                    labelText: "Wprowadź nowe hasło"),
                textAlign: TextAlign.center,
                onChanged: (text) {
                  password_to_change = text;
                },
              ),
              TextField(
                obscureText: true,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
                decoration: InputDecoration(
                    fillColor: Colors.white,
                    filled: true,
                    labelText: "Wprowadź nowe hasło ponownie"),
                textAlign: TextAlign.center,
                onChanged: (text) {
                  password_to_change_again = text;
                },
              ),
              TextButton(
                child: Text(
                  "Zmień hasło",
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                onPressed: () {
                  setState(() {
                    change_password(password_to_change_old_password,
                        password_to_change, password_to_change_again);
                  });
                },
              ),
              Visibility(
                child: Text(
                  error_box_text,
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
                visible: error_box_visibility,
              ),
              TextButton(
                child: Text("Nie pamiętasz hasła - zresetuj je.",
                    style: TextStyle(color: Colors.white)),
                onPressed: () {
                  //Przejście do widoku resetu hasła za pomocą e-maila
                  change_password_box = false;
                  reset_password_box = true;
                  setState(() {});
                },
              ),
              IconButton(
                  icon: Icon(Icons.arrow_back_ios),
                  onPressed: () {
                    //Powrót do widoku zalogowanego usera
                    change_password_box = false;
                    setState(() {});
                  })
            ],
          );
        }
        //Po zalogowaniu bez zmiany hasła
        else {
          return Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                          color: Colors.amber,
                          borderRadius: BorderRadius.circular(10)),
                      alignment: Alignment.topLeft,
                      child: Text(
                        username,
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 18),
                      ),
                    ),
                    Container(
                      alignment: Alignment.topRight,
                      child: FlatButton(
                        child: Icon(Icons.logout, color: Colors.amber),
                        onPressed: () {
                          Login();
                          setState(() {});
                        },
                      ),
                    ),
                  ],
                ),
                Spacer(
                  flex: 1,
                ),
                FlatButton(
                  color: Colors.amber,
                  child: Text(
                    "Zmiana hasła",
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                  onPressed: () {
                    change_password_box = true;
                    setState(() {});
                  },
                ),
                FlatButton(
                  color: Colors.amber,
                  child: Text(
                    "Twoje stacje",
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => StationView()));
                  },
                ),
              ]));
        }
      }
      //Widget logowania przed zalogowaniem
      else {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
              decoration: InputDecoration(
                  fillColor: Colors.white, filled: true, labelText: "E-mail"),
              key: Key("Login_Field"),
              textAlign: TextAlign.center,
              onChanged: (text) {
                username = text;
              },
            ),
            TextField(
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
              decoration: InputDecoration(
                fillColor: Colors.white,
                filled: true,
                labelText: "Hasło",
              ),
              key: Key("Password_Field"),
              obscureText: true,
              textAlign: TextAlign.center,
              onChanged: (text) {
                password = text;
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Checkbox(
                  value: remember_login,
                  onChanged: (checked) {
                    setState(() {
                      remember_login = checked;
                    });
                  },
                ),
                Text("Zapamiętaj mnie")
              ],
            ),
            Visibility(
              child: Text(
                error_box_text,
                style: TextStyle(
                    color: Colors.red,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
              visible: error_box_visibility,
            ),
            Visibility(
              child: TextButton(
                child: Text("Nie pamiętasz hasła?",
                    style: TextStyle(color: Colors.white)),
                onPressed: () {
                  reset_password_box = false;
                  setState(() {});
                },
              ),
              visible: reset_password_btn_visibility,
            ),
            FlatButton(
              color: Colors.white,
              child: Text(
                "Zaloguj",
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              ),
              onPressed: () {
                Login();
                setState(() {});
              },
            ),
            TextButton(
              child: Text("Nie masz konta - zarejestruj się",
                  style: TextStyle(color: Colors.white)),
              onPressed: () {
                change_between_login_register();
                setState(() {});
              },
            )
          ],
        );
      }
    }
    //Widget rejestracji
    else if (login_or_register == "register") {
      //Widget rejestracji
      return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Container(
          height: 15,
        ),
        TextField(
          style: TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
          decoration: InputDecoration(
              fillColor: Colors.white, filled: true, labelText: "E-mail"),
          key: Key("R_Login_Field"),
          textAlign: TextAlign.center,
          onChanged: (text) {
            user_to_register_login = text;
          },
        ),
        TextField(
          obscureText: true,
          style: TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
          decoration: InputDecoration(
              fillColor: Colors.white, filled: true, labelText: "Hasło"),
          key: Key("R_Password_Field"),
          textAlign: TextAlign.center,
          onChanged: (text) {
            password_to_register = text;
          },
        ),
        TextField(
          obscureText: true,
          style: TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
          decoration: InputDecoration(
              fillColor: Colors.white,
              filled: true,
              labelText: "Wpisz hasło ponownie"),
          key: Key("R_Password_Again_Field"),
          textAlign: TextAlign.center,
          onChanged: (text) {
            password_to_register_again = text;
          },
        ),
        Container(
          height: 5,
        ),
        Visibility(
          visible: error_box_visibility,
          child: Text(
            error_box_text,
            style: TextStyle(
                color: Colors.red, fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        FlatButton(
          color: Colors.white,
          textColor: Colors.green,
          child: Text(
            "Zarejestruj",
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18),
          ),
          onPressed: () {
            Register();
            setState(() {});
          },
        ),
        TextButton(
          child: Text("Masz już konto - zaloguj się",
              style: TextStyle(color: Colors.white)),
          onPressed: () {
            change_between_login_register();
            setState(() {});
          },
        )
      ]);
    }
  }

  Widget build(BuildContext context) {
    CONTEXT = context;
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title, style: headTextStyle),
          centerTitle: true,
          backgroundColor: Theme.of(context).appBarTheme.color,
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => MenuDashbord()));
            },
            icon: Icon(
              Icons.menu,
              size: 45,
            ),
          ),
          actions: [
            FlatButton(
              textColor: Colors.white,
              child: Row(children: [
                Text(username_to_display),
                Icon(Icons.supervised_user_circle)
              ]),
              onPressed: () => Bottom_Panel_Builder(),
            ),
          ],
        ),
        body: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(10),
              child: Column(
                children: [
                  Text(
                    "TUTAJ BĘDZIE MAPA (to jest tylko placeholder, " +
                        "jeśli chcesz zobaczyć webview z mapą odkomentuj " +
                        "poniższe linie w kodzie)",
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ),
            //WEBVIEW do map (tak to to trzebe odkomentować)
            // WebView(
            //   initialUrl: "https://www.windy.com/?" +
            //       lon.toString() +
            //       "," +
            //       lat.toString() +
            //       ",10,m:e7Uagbb",
            //   javascriptMode: JavascriptMode.unrestricted,
            // ),
            SlidingUpPanel(
              // border: Border(top: BorderSide(width: 10,color: e),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20)),
              color: Theme.of(context).primaryColor,
              controller: sliding_up_panel_controller,
              minHeight: 0,
              maxHeight: 350,
              panel: Center(
                child: login_register_widget(),
              ),
            )
          ],
        ));
  }
}
