import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'variables.dart';
import 'main.dart';
import 'package:location/location.dart';

//W tym pliku znajduje się cała mechanika odpowiedzialna za logowanie, rejestrację
//zmianę hasła, reset hasła itd.

class Response {
  int id;
  String desc;

  Response(this.id, this.desc);

  factory Response.fromJson(dynamic json) {
    return Response(json[0]['ID'] as int, json[0]['desc'] as String);
  }

  @override
  String toString() {
    return '{ ${this.id}, ${this.desc} }';
  }
}

void Login() async {
  if (is_logged) {
    print("WyLOgowando");
    is_logged = false;
    password = "";
    username = "";
    username_to_display = "";
    remember_login = false;
  } else {
    print("LOgowando");
    //próba logowanie
    String url = "http://" +
        API_SERWER_IP +
        "/login?email=" +
        username +
        "&password=" +
        password +
        "&api_key=" +
        LOGIN_REQUEST_API_KEY;

    Map map = {};
    Response odp = await apiRequest(url, map);
    print(odp.id);
    if (odp.id == 100) {
      print("Logowanie poprawne");
      username_to_display = username;
      error_box_visibility = false;
      is_logged = true;
      reset_password_btn_visibility = false;
    } else if (odp.id == 200) {
      print("Logowanie niepoprawne");
      error_box_text = "Złe dane logowania";
      error_box_visibility = true;
      reset_password_btn_visibility = true;
    }
  }
}

void change_between_login_register() {
  if (login_or_register == "login") {
    error_box_visibility = false;
    reset_password_btn_visibility = false;
    login_or_register = "register";
  } else {
    error_box_visibility = false;
    reset_password_btn_visibility = false;
    login_or_register = "login";
  }
}

bool check_if_mail_valid(String mail) {
  Pattern pattern = r"^[a-zA-Z0-9_-]{3,20}@[a-zA-Z0-9]{1,100}.*";
  RegExp regex = new RegExp(pattern);
  if (regex.hasMatch(mail)) {
    return true;
  } else
    return false;
}

bool check_if_password_valid(String password) {
  Pattern pattern = r"^[A-Z]{1}[a-z]{3,}[0-9]{1,}[$&+,:;=?@#|'<>.-^*()%!]{1,}";
  RegExp regex = new RegExp(pattern);
  if (regex.hasMatch(password)) {
    return true;
  } else
    return false;
}

void Register() async {
  print("Rejestracja");
  print(user_to_register_login);
  if (check_if_mail_valid(user_to_register_login) == false ||
      user_to_register_login == "") {
    error_box_text = "Wpisany adres mail jest niepoprawny";
    error_box_visibility = true;
  } else {
    if (password_to_register != "") {
      if (password_to_register == password_to_register_again) {
        //Jeśli wpisane hasła w pola są identyczne
        error_box_visibility = false;
        String url = "http://" +
            API_SERWER_IP +
            "/register?email=" +
            user_to_register_login +
            "&password=" +
            password_to_register +
            "&lat=" +
            lat.toString() +
            "&lon=" +
            lon.toString() +
            "&api_key=" +
            REGISTER_REQUEST_API_KEY;
        Map map = {};
        Response odp = await apiRequest(url, map);
        print(odp.id);
        if (odp.id == 100) {
          print("Rejestracja przebiegła pomyślnie");
          username_to_display = username;
          error_box_visibility = false;
          is_logged = true;
          username = user_to_register_login;
          username_to_display = username;
          login_or_register = "login";
        } else if (odp.id == 200) {
          print("Rejestracja niepoprawna");
          error_box_text = "Podany adres występuje już w naszej bazie";
          error_box_visibility = true;
        }
      } else {
        //Jeśli wpisane hasła nie są identyczne
        error_box_text = "Wpisane hasła nie są takie same";
        error_box_visibility = true;
      }
    } else {
      error_box_text = "Nie można zostawić pola hasła pustego";
      error_box_visibility = true;
    }
  }
}

Future<Response> apiRequest(String url, Map jsonMap) async {
  HttpClient httpClient = new HttpClient();
  HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
  request.headers.set('content-type', 'application/json');
  request.add(utf8.encode(json.encode(jsonMap)));
  HttpClientResponse response = await request.close();
  // todo - you should check the response.statusCode
  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON
    String reply = await response.transform(utf8.decoder).join();
    Response rr = Response.fromJson(jsonDecode(reply));
    httpClient.close();
    return rr;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}

void change_password(
    String old_password, String new_password, String new_password_again) async {
  if (new_password == new_password_again && new_password != "") {
    // Zapytanie czy stare hasło jest zgodne z tym co w bazie
    String url = "http://" +
        API_SERWER_IP +
        "//change_password?email=" +
        username +
        "&password=" +
        old_password +
        "&new_password=" +
        new_password +
        "&api_key=" +
        CHANGE_PASSWORD_API_KEY;

    Map map = {};
    Response odp = await apiRequest(url, map);
    print(odp);
    if (odp.id == 100) {
      error_box_visibility = false;
      change_password_box = false;
    } else if (odp.id == 200) {
      error_box_text = "Wpisane stare hasło nie jest poprawne";
      error_box_visibility = true;
    }
  } else {
    error_box_text = "Wprowadzone hasła nie są identyczne !";
    error_box_visibility = true;
  }
}

Future<void> _shownormalDialog(
  String title,
  String text,
) async {
  return showDialog<void>(
    context: CONTEXT,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(title),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(text),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: Text('OK'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

void reset_password(String mail) async {
  if (mail != "") {
    String url = "http://" +
        API_SERWER_IP +
        "//forgot_password?email=" +
        mail +
        "&api_key=" +
        FORGOT_PASSWORD_API_KEY;

    Map map = {};
    Response odp = await apiRequest(url, map);
    if (odp.id == 100) {
      error_box_visibility = false;
      change_password_box = false;
      _shownormalDialog(
          "Info", "Sprawdź swojego maila, nowe hasło zostało tam wysłane.");
    } else if (odp.id == 200) {
      error_box_text = "Podany mail nie występuje w naszej bazie";
      error_box_visibility = true;
    }
  } else {
    error_box_text = "Pozostawiłeś pole mail puste, tak nie wolno 😡";
    error_box_visibility = true;
  }
}
