///Notyfication for system bar in Android///
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

///Przykładowe użycie
///final Notifications _notifications =  Notifications();
///@override
///  void initState() {
///    super.initState();
///    this._notifications.initNotifications(); // initialise notification
///  }
///void _pushNotification() {
///    this._notifications.pushNotification('Brak danych', 'Czy jesteś połączony z Internetem?'); // display notification
///  }
///
class Notifications {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  void initNotifications() async {
    print("Inicializacja");
    final AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('Logo');
    final IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings(onDidReceiveLocalNotification: null);
    final MacOSInitializationSettings initializationSettingsMacOS =
        MacOSInitializationSettings();
    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS,
            macOS: initializationSettingsMacOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);
  }

  Future<void> pushNotifications(String title, String content) async {
    print("Wysłana");
    const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
      'push_messages: 0',
      'push_messages: powiadomienie',
      'push_messages: Weather Forecast And Storm Prediction',
      importance: Importance.max,
      priority: Priority.high,
      showWhen: false,
      enableVibration: true,
    );
    const NotificationDetails platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      0,
      title,
      content,
      platformChannelSpecifics,
    );
  }

  Future selectNotification(String text) async {
    //
  }
}
